# script to clone all repositories 
# chmod 777 clone_repo.sh
# ./clone_repo.sh

echo && echo $i Type 10
read number

# type 10 to proceed : to avoid mistake
if [ $number -ne 10 ] 
then
    echo && echo $i Type 10 to proceed
else

    cd ..  # go one folder back and then clone

    i=1  # start from 1 as 1_commands_for_repo is already cloned

    ################### Github repos  ###################

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://github.com/PythonDSP/FPGA-designs-with-Verilog-and-SystemVerilog

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://github.com/PythonDSP/FPGA-designs-with-VHDL

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://github.com/PythonDSP/Meher-Baba-prayers-kindle-format

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://github.com/PythonDSP/Programming-with-C-and-CPP


    ################### Bitbucket repos ###################

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/7-Signal-and-Systems

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/8-Digital-Communication

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/9-Matrix

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/cpp-guide

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Cython_ReadTheDoc

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/dataminingguide 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/django-guide    

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/django11-delete     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/docguide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/flask-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://github.com/PythonDSP/gitguide.git

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/goodread 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/html-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/javaguide  

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Latex-Format-for-Thesis-and-Notes 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Linux-software-list 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/machine-learning-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/machinelearning     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/matplotlibguide 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Multiuser-Detection     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/myhdlguide

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/mysqlguide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pandasguide

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/postgresql     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pyref     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pytestGuide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/python-with-turtle      

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Python3-Simulation-with-OOP 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/python_gui

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pythondsp

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pythonguide

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pythonic     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/pythonIntermediate     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/regexguide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/ros-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/simulationGuide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/solutionWD     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/Sphinx-Format-for-LatexPDF-HTML 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/sublimeguide 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/tmux-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/unixguide 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/verilogguide 

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/VHDL-guide     

    i=$(expr $i + 1)
    echo && echo $i # enter a blank line
    git clone --depth 1 https://mpat260@bitbucket.org/pythondsp/vimguide 

fi
